# source: opencv documentation (https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_calib3d/py_calibration/py_calibration.html#calibration)

import numpy as np
import cv2
import glob

calib_settings_dir = "./calib_settings_medium/"

frame_resolution = (3280,2464)
chessboard_size = (9,6) #number of boxes on board, start counting at 0

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)


# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((chessboard_size[0] * chessboard_size[1], 3), np.float32)
objp[:,:2] = np.mgrid[0:chessboard_size[0],0:chessboard_size[1]].T.reshape(-1,2)*0.8


# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob('./chessboard_pics_medium/*.jpeg')

for image in images:
    img = cv2.imread(image)
    #print(img)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, chessboard_size, None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)

        corners2 = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)

        # Draw and display the corners
        cv2.drawChessboardCorners(img, chessboard_size, corners2, ret)
        cv2.imshow('img',img)
        cv2.waitKey(500)

cv2.destroyAllWindows()

# Calibration part
ret, camera_matrix, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, frame_resolution, None, None)
print(camera_matrix)

np.save(calib_settings_dir+'cam_mtx.npy', camera_matrix)
np.save(calib_settings_dir+'dist.npy', dist)

# Undistortion
img = cv2.imread('./chessboard_pics_medium/chessboard_7.jpeg')
h,  w = img.shape[:2]
print(h, w)
newcameramatrix, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist, (w,h), 1, (w,h))

np.save(calib_settings_dir+'newcam_mtx.npy', newcameramatrix)
np.save(calib_settings_dir+'roi.npy', roi)

print('roi: ', roi)

# undistort
#st = cv2.undistort(img, camera_matrix, dist, None, newcameramatrix)
# crop the image
#x, y, w, h = roi
#dst = dst[y:y+h, x:x+w]
#cv2.imwrite('./calibresult.png', dst)


# undistort
mapx, mapy = cv2.initUndistortRectifyMap(camera_matrix, dist, None, newcameramatrix, (w,h), 5)
dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)
# crop the image
x, y, w, h = roi
dst = dst[y:y+h, x:x+w]
cv2.imwrite('./calibresult_medium.png', dst)



#Re-projection Error
mean_error = 0
for i in range(len(objpoints)):
    imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], camera_matrix, dist)
    error = cv2.norm(imgpoints[i], imgpoints2, cv2.NORM_L2)/len(imgpoints2)
    mean_error += error

print("total error: {}".format(mean_error/len(objpoints)))

