import numpy as np
import cv2

calib_settings_dir = "./calib_settings_medium/"

camera_matrix = np.load(calib_settings_dir+'cam_mtx.npy')
dist = np.load(calib_settings_dir+'dist.npy')
newcameramatrix = np.load(calib_settings_dir+'newcam_mtx.npy')

image = cv2.imread('./chessboard_pics_medium/chessboard_18.jpeg')

image_undst = cv2.undistort(image, camera_matrix, dist, None, newcameramatrix)
while (1):
    cv2.imshow('undistorted', image_undst)
    k=cv2.waitKey(5)
    if k==27:
        break

cv2.destroyAllWindows