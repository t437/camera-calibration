## Camera Calibration

**calibrate.py** follows very closely the OpenCV documentation and saves the output matrices.

Every time a new camera is used the calibration should be performed and, for this, new pictures of the printed chessboard (chessboard.png) should be taken with it. Read about this in the thesis or mentioned sources below.

**calibration_settings_medium** is where the camera calibration files are stored (calibration_Settings_small was a test of camera calibration using a smaller printed chessboard and less images, results were better with more images and a larger chessboard).

**initial_perspective_calibration.py** calibrate camera and perspective. Furthermore, checks accuracy of calibration.

**realXYZ.py** takes the u,v pixel points of the object detected and translates it to realy world coordinates.

Sources:

Garcia P. (2019) Calculate X, Y, Z Real World Coordinates from Image Coordinates using OpenCV. Published on fdxlabs.com [online] https://www.fdxlabs.com/calculate-x-y-z-real-world-coordinates-from-a-single-camera-using-opencv/ [21.10.2021]

https://medium.com/@pacogarcia3 computer-vision-pick-and-place-for-toys-using-raspberry-pi-and-arduino-68a04874c039

Calibration
https://docs.opencv.org/3.3.0/dc/dbb/tutorial_py_calibration.html

Function Docs
https://docs.opencv.org/3.3.0/d9/d0c/group__calib3d.html#ga93efa9b0aa890de240ca32b11253dd4a


